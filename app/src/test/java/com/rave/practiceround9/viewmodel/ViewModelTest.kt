package com.rave.practiceround9.viewmodel

import com.rave.practiceround9.model.BobRepo
import com.rave.practiceround9.model.local.BobCharacter
import com.rave.practiceround9.util.CoroutinesTestExtension
import com.rave.practiceround9.util.InstantTaskExecutorExtension
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.RegisterExtension

@OptIn(ExperimentalCoroutinesApi::class)
@ExtendWith(InstantTaskExecutorExtension::class)
internal class ViewModelTest {

    @RegisterExtension
    val testExtension = CoroutinesTestExtension()

    private val repo: BobRepo = mockk()
    private val viewModel: BobViewModel = BobViewModel(repo)

    @Test
    fun testInitialValue() = runTest(testExtension.dispatcher) {
        val characters = viewModel.characters.value
        Assertions.assertTrue(characters.isNullOrEmpty())
    }

    @Test
    fun testValueChange() = runTest(testExtension.dispatcher) {
        val expectedValue = listOf(
            BobCharacter(
                id = 1,
                name = "Bob"
            )
        )

        // Given
        coEvery { repo.getBobCharacters() } coAnswers { expectedValue }

        // When
        viewModel.getBobCharacters()

        // Then
        val characters = viewModel.characters.value
        Assertions.assertEquals(characters, expectedValue)
    }
}
