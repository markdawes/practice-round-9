package com.rave.practiceround9.model

import com.rave.practiceround9.model.local.BobCharacter
import com.rave.practiceround9.model.remote.APIService
import com.rave.practiceround9.model.remote.dtos.BobCharacterDTO
import com.rave.practiceround9.util.CoroutinesTestExtension
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension

@OptIn(ExperimentalCoroutinesApi::class)
internal class RepoTest {

    @RegisterExtension
    private val testExtension = CoroutinesTestExtension()
    private val mockService: APIService = mockk()
    private val repo = BobRepo(mockService)

    @Test
    fun testGetCharacters() = runTest(testExtension.dispatcher) {
        // Given
        val result = listOf(
            BobCharacterDTO(
                age = "12",
                firstEpisode = "1",
                gender = "male",
                hairColor = "blonde",
                id = 1,
                image = "blah",
                name = "Bob",
                occupation = "cook",
                relatives = null,
                url = null,
                voicedBy = null,
                wikiUrl = null
            )
        )
        val expectedResult = listOf(
            BobCharacter(
                id = 1,
                name = "Bob"
            )
        )
        coEvery { mockService.getBobCharacters() } coAnswers { result }

        // When
        val characters = repo.getBobCharacters()

        // Then
        Assertions.assertEquals(expectedResult, characters)
    }
}
