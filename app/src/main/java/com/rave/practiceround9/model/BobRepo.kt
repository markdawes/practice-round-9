package com.rave.practiceround9.model

import com.rave.practiceround9.model.local.BobCharacter
import com.rave.practiceround9.model.remote.APIService
import javax.inject.Inject

/**
 * Bob repo.
 *
 * @property service
 * @constructor Create empty Bob repo
 */
class BobRepo @Inject constructor(private val service: APIService) {
    /**
     * Get bob characters.
     *
     * @return
     */
    suspend fun getBobCharacters(): List<BobCharacter> {
        val characterDTOs = service.getBobCharacters()
        return characterDTOs.map {
            BobCharacter(
                id = it.id!!,
                name = it.name!!
            )
        }
    }
}
