package com.rave.practiceround9.model.remote

import com.rave.practiceround9.model.remote.dtos.BobCharacterDTO
import retrofit2.http.GET

/**
 * Api service to connect to endpoints.
 *
 * @constructor Create empty A p i service
 */
interface APIService {

    @GET(CHARACTER_ENDPOINT)
    suspend fun getBobCharacters(): List<BobCharacterDTO>

    companion object {
        private const val CHARACTER_ENDPOINT = "characters/"
    }
}
