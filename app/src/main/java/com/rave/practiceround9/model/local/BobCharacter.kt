package com.rave.practiceround9.model.local

/**
 * Bob character.
 *
 * @property id
 * @property name
 * @constructor Create empty Bob character
 */
data class BobCharacter(
    val id: Int,
    val name: String
)
