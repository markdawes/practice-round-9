package com.rave.practiceround9.model.remote.dtos

import kotlinx.serialization.Serializable

@Serializable
class BobCharacterResponse : ArrayList<BobCharacterDTO>()
