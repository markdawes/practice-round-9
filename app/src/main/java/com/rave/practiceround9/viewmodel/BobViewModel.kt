package com.rave.practiceround9.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.practiceround9.model.BobRepo
import com.rave.practiceround9.model.local.BobCharacter
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Bob view model.
 *
 * @property repo
 * @constructor Create empty Bob view model
 */
@HiltViewModel
class BobViewModel @Inject constructor(private val repo: BobRepo) : ViewModel() {
    private val _characters: MutableLiveData<List<BobCharacter>> = MutableLiveData()
    val characters: LiveData<List<BobCharacter>> get() = _characters

    /*init {
        getBobCharacters()
    }*/

    /**
     * Get bob characters.
     *
     */
    fun getBobCharacters() = viewModelScope.launch {
        _characters.value = repo.getBobCharacters()
    }
}
