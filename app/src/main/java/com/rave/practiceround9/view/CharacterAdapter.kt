package com.rave.practiceround9.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rave.practiceround9.databinding.ItemCharacterBinding
import com.rave.practiceround9.model.local.BobCharacter

/**
 * Character adapter.
 *
 * @constructor Create empty Character adapter
 */
class CharacterAdapter : RecyclerView.Adapter<CharacterAdapter.CharacterViewHolder>() {

    private var characterList: List<BobCharacter> = emptyList()

    /**
     * Character view holder.
     *
     * @property binding
     * @constructor Create empty Character view holder
     */
    inner class CharacterViewHolder(private val binding: ItemCharacterBinding) : RecyclerView.ViewHolder(binding.root) {
        /**
         * Display character.
         *
         * @param character
         */
        fun displayCharacter(character: BobCharacter) = with(binding) {
            tvCharacter.text = character.name
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return CharacterViewHolder(
            ItemCharacterBinding.inflate(inflater, parent, false)
        )
    }

    override fun onBindViewHolder(holder: CharacterViewHolder, position: Int) {
        holder.displayCharacter(characterList[position])
    }

    override fun getItemCount(): Int {
        return characterList.size
    }

    /**
     * Set data.
     *
     * @param characters
     */
    fun setData(characters: List<BobCharacter>) {
        this.characterList = characters
        notifyDataSetChanged()
    }
}
