package com.rave.practiceround9.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.rave.practiceround9.databinding.FragmentCharacterBinding
import com.rave.practiceround9.viewmodel.BobViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * A simple [Fragment] subclass.
 */
@AndroidEntryPoint
class CharacterFragment : Fragment() {

    private var _binding: FragmentCharacterBinding? = null
    private val binding get() = _binding!!

    private val viewModel by viewModels<BobViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return FragmentCharacterBinding.inflate(inflater, container, false).apply {
            _binding = this
            val adapter = CharacterAdapter()
            viewModel.characters.observe(
                viewLifecycleOwner,
                Observer {
                    adapter.setData(it)
                }
            )
            binding.rvCharacters.adapter = adapter
            binding.rvCharacters.layoutManager = LinearLayoutManager(requireContext())
        }.root
    }
}
