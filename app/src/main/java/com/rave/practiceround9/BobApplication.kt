package com.rave.practiceround9

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Main class for dagger hilt.
 *
 * @constructor Create empty Bob application
 */
@HiltAndroidApp
class BobApplication : Application()
